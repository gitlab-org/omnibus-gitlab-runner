#
## Copyright:: Copyright (c) 2015 GitLab B.V.
## License:: Apache License, Version 2.0
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
## http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
#

name "gitlab-runner"
default_version "3ba62db6a6d5f2f457e02f2b408a251fb74d3dad" # master 2015-02-16

dependency "ruby"
dependency "rubygems"
dependency "bundler"
dependency "libicu"
dependency "git"

source :git => "https://gitlab.com/gitlab-org/gitlab-ci-runner.git"

build do
  env = with_standard_compiler_flags(with_embedded_path)
  bundle "install --jobs #{workers} --path=#{install_dir}/embedded/service/gem", env: env

  runner_install_dir = "#{install_dir}/embedded/service/gitlab-runner"
  command "mkdir -p #{runner_install_dir}"
  sync project_dir, runner_install_dir, exclude: '.git'
  command "ln -s #{runner_install_dir}/bin/* #{install_dir}/bin/"

  block do
    env_shebang = "#!/usr/bin/env ruby"
    `grep -r -l '^#{env_shebang}' #{project_dir}`.split("\n").each do |ruby_script|
      script = File.read(ruby_script)
      erb :dest => ruby_script.sub(project_dir, "#{install_dir}/embedded/service/gitlab-runner"),
        :source => "ruby_script_wrapper.erb",
        :mode => 0755,
        :vars => {:script => script, :install_dir => install_dir}
    end
  end
end

#
## Copyright:: Copyright (c) 2015 GitLab B.V.
## License:: Apache License, Version 2.0
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
## http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
#

name "documentation"

# Use a Git checksum of the doc/ directory as the 'version' to work with
# omnibus caching
version `git ls-tree HEAD -- doc | awk '{ print $3 }'`

source path: File.expand_path("doc", Omnibus::Config.project_root)

build do
  command "mkdir -p #{install_dir}/doc"
  sync '.', "#{install_dir}/doc"

  block do
    open(File.join(install_dir, 'README'), 'w') do |f|
    f.puts <<EOS
GitLab Runner
============

Please see #{install_dir}/doc for documentation.
EOS
    end
  end
end

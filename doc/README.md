# Omnibus packages for GitLab Runner

Deprecated in favor of https://gitlab.com/gitlab-org/gitlab-ci-multi-runner .

- [Installation instructions](install/README.md)
- [Build instructions](build/README.md)

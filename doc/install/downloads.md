# GitLab Runner downloads

- Ubuntu 12.04 64-bit [gitlab-runner_5.2.0~omnibus.1-1_amd64.deb](https://s3-eu-west-1.amazonaws.com/downloads-packages/ubuntu-12.04/gitlab-runner_5.2.0~omnibus.1-1_amd64.deb) SHA256 a9c44105b724dcd32d63a7d32fc97d7ceec6c35829fae7336d35340b6d839b44
- Ubuntu 14.04 64-bit [gitlab-runner_5.2.0~omnibus.1-1_amd64.deb](https://s3-eu-west-1.amazonaws.com/downloads-packages/ubuntu-14.04/gitlab-runner_5.2.0~omnibus.1-1_amd64.deb) SHA256 fcce24abd84dd4c54aa6127a2bb2788c1b3af8dd5076def26880a789fd950316
- Debian 7 64-bit [gitlab-runner_5.2.0~omnibus.1-1_amd64.deb](https://s3-eu-west-1.amazonaws.com/downloads-packages/debian-7.8/gitlab-runner_5.2.0~omnibus.1-1_amd64.deb) SHA256 d3f9ab34a3273f3f4a44e7a95d9ad8368d47d5f5def24d7338425b38338dd4a8
- Enterprise Linux 6 64-bit [gitlab-runner-5.2.0~omnibus.1-1.x86_64.rpm](https://s3-eu-west-1.amazonaws.com/downloads-packages/centos-6.6/gitlab-runner-5.2.0~omnibus.1-1.x86_64.rpm) SHA256 644152f3c8094dd0cc3f50ebeb1bc921187e65462af8b1290e725c5767263a21
- Enterprise Linux 7 64-bit [gitlab-runner-5.2.0~omnibus.1-1.x86_64.rpm](https://s3-eu-west-1.amazonaws.com/downloads-packages/centos-7.0.1406/gitlab-runner-5.2.0~omnibus.1-1.x86_64.rpm) SHA256 687f3f85ffd829246ae16b4d1c5ed73b4a4150a24eb64aef4b466dde0edba792

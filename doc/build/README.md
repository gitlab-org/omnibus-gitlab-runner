# Building omnibus-gitlab-runner packages

Start with the [setup for omnibus-gitlab builders](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/doc/build.md).

Then add:

```
sudo mkdir -p /opt/gitlab-runner
sudo chown omnibus-build:omnibus-build /opt/gitlab-runner
sudo mkdir -p /var/cache/omnibus
sudo chown omnibus-build:omnibus-build /var/cache/omnibus
```

For Centos 6 and 7, add:

```
sudo yum install fakeroot
```

Now you can build a package by running:

```
sudo -iu omnibus-build
./attach.sh
cd omnibus-gitlab-runner
# checkout the desired version
./release.sh
```
